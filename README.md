#Spaceships and Asteroids
The object of the game is to achieve the highest score possible by destroying as many enemy spaceships and asteroids as you can. As your score increases, the number of enemy ships increases, and they fire more bullets.

##Technology
Spaceships and Asteroids uses the HTML5 canvas element to create the game field. The game incorporates the createJS JavaScript library to create a stage inside the canvas in which the gameplay happens.  CreateJS is useful because greatly simplifies the drawing of sprites onto the game's stage, and an addon library exists for easy pixel-level collision detection.
